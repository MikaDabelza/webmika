from django.db import models

# Create your models here.
class JadwalPribadi(models.Model):
    tanggal = models.DateField()
    jam = models.TimeField()
    nama = models.CharField(max_length = 100)
    tempat = models.CharField(max_length = 100)
    kategori = models.CharField(max_length = 100)

class meta:
    ordering = ['tanggal', 'jam']

from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index, name = 'home'),
    path('About/', views.about, name = 'about'),
    path('Contact/', views.contact, name = 'contact'),
    path('Register/', views.register, name = 'Register'),
    path('PageRegister/', views.pageRegister, name = 'PageRegister'),
    path('delete/',views.delete, name = 'deleteDB'),
    path('Books/',views.Books, name = 'Books'),
    
]

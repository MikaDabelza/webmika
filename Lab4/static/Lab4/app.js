


$(document).ready(function () {
    $(function () {
        $("#accordion").accordion();
    });
    $("#buttontema").click(function () {
        $("#navbarcoy").toggleClass("navbarganti");
        $(".contentBG").toggleClass("BGganti");
        $(".littleStoring").toggleClass("littleStoringGanti");
    });
  

});

$(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");;
});

$("#tombolSearch").click( function () {
    var isikotak = $('#kotakSearch').val();
    
    console.log('https://www.googleapis.com/books/v1/volumes?q='+ isikotak)
    $.ajax({
        type: "GET",
        url: 'https://www.googleapis.com/books/v1/volumes?q='+ isikotak,
        dataType: 'json',
        context: document.body,
        success: function (data) {
            console.log("uwuuu")
            $("#isi_table").empty();
            $.each(data["items"], function (i, book) {
                $("#isi_table").append(
                    `
                        <tr style: "background-color: rgb(188, 255, 240);">
                            <td>${book["volumeInfo"]["title"]}</td>
                            <td>${book["volumeInfo"]["authors"]}</td>
                            <td>${book["volumeInfo"]["publishedDate"]}</td>
                            <td>
                                <img id="${book["id"]}" class="notfavorited" src="${not_fav}" alt=""
                                    width="36">
                            </td>

                        </tr>
                    `
                )
            })
        }
    });
});

$(document).on("click", ".notfavorited", function (e) {
    console.log("star clicked")
    $(this).toggleClass("notfavorited");
    $(this).toggleClass("favorited");
    $(this).attr('src', fav);
    $('#jmlFav').text(function (idx, txt) {
        return +txt + 1;
    });
});

$(document).on("click", ".favorited", function (e) {
    console.log("star clicked")
    $(this).toggleClass("notfavorited");
    $(this).toggleClass("favorited");
    $(this).attr('src', not_fav);
    $('#jmlFav').text(function (idx, txt) {
        return +txt - 1;
    });
});
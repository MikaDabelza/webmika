from django.shortcuts import render
from django.shortcuts import redirect
from Lab4.forms import JadwalForm
from Lab4.models import JadwalPribadi
from django.http import JsonResponse



# Create your views here.
def index(request):
    return render(request, 'Lab4/Story3.html')
def about(request):
    return render(request, 'Lab4/About.html')
def contact(request):
    return render(request, 'Lab4/Contact.html')
def register(request):
    Jadwals = JadwalPribadi.objects.all()
    if request.method == 'POST':
        formPost = JadwalForm(request.POST)
        if formPost.is_valid():
            formPost.save()

            formPost = JadwalForm()
            context = {'JadwalForm':formPost,
                        'Jadwals' : Jadwals}

            return redirect('/PageRegister/')
    else:
        formPost = JadwalForm()
    context = {'JadwalForm':formPost,
                'Jadwals' : Jadwals}
    return redirect('/PageRegister/')

def pageRegister(request):
    Jadwals = JadwalPribadi.objects.all()
    formPost = JadwalForm(request.POST)
    context = {'JadwalForm':formPost,
                'Jadwals' : Jadwals}
    return render(request, 'Lab4/formJadwal.html', context)

def delete(request):
    JadwalPribadi.objects.all().delete()
    return redirect('/PageRegister/')
    

def Books(request):
    return render(request, 'Lab4/Books.html')


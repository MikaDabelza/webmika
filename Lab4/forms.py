from django import forms
from Lab4.models import JadwalPribadi

class JadwalForm(forms.ModelForm):
    class Meta:
        model = JadwalPribadi
        fields = ['tanggal', 'jam', 'nama', 'tempat', 'kategori']
        widgets = {
            'tanggal' : forms.DateInput(attrs = {'type': 'date','class': 'form-control'}),
            'jam' : forms.TimeInput(attrs = {'type' : 'time', 'class':'form-control'}),
            'nama' : forms.TextInput(attrs = {'class':'form-control'}),
            'tempat' : forms.TextInput(attrs = {'class':'form-control'}),
            'kategori' : forms.TextInput(attrs = {'class':'form-control'})
        
        }

    